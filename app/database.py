import os
from config import settings
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Boolean, Integer, String, ForeignKey


engine = create_engine(settings.db_url)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)

    votes = relationship("Vote", back_populates="user")

class Vote(Base):
    __tablename__ = "votes"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    snapshot_id = Column(Integer, ForeignKey("snapshots.id"))
    option = Column(Boolean)

    user = relationship("User", back_populates="votes")
    snapshot = relationship("Snapshot", back_populates="votes")

class Snapshot(Base):
    __tablename__ = "snapshots"

    id = Column(Integer, primary_key=True, index=True)
    img_file_name = Column(String)
    ref_image_file_name = Column(String)

    votes = relationship("Vote", back_populates="snapshot")

Base.metadata.create_all(bind=engine)
