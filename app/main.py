from typing import List
from sqlalchemy import func, desc
from sqlalchemy.orm import Session
from pydantic import BaseModel
from fastapi import FastAPI, Depends, Form, Request, HTTPException, status
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse, FileResponse, RedirectResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi_login.exceptions import InvalidCredentialsException
from fastapi_login import LoginManager

from database import Base, SessionLocal, User as dbUser, Vote as dbVote, Snapshot as dbSnapshot


class NotAuthenticatedException(Exception):
    pass


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")

SECRET = "super-secret-key"
manager = LoginManager(SECRET, '/api/login', use_cookie=True,
                       custom_exception=NotAuthenticatedException)


Sessions = {}


@manager.user_loader()
def load_user(user_id: str):
    if user_id in Sessions:
        return Sessions[user_id]
    raise NotAuthenticatedException()


@app.exception_handler(NotAuthenticatedException)
def auth_exception_handler(request: Request, exc: NotAuthenticatedException):
    """
    Redirect the user to the login page if not logged in
    """
    return RedirectResponse(url='/login')


class SnapshotCreate(BaseModel):
    img_file_name: str
    ref_image_file_name: str


class UserCreate(BaseModel):
    username: str
    password: str


class Snapshot(BaseModel):
    id: int
    img_file_name: str
    ref_image_file_name: str

    class Config:
        orm_mode = True


class User(BaseModel):
    username: str

    class Config:
        orm_mode = True


class Vote(BaseModel):
    option: bool
    snapshot_id: int

    class Config:
        orm_mode = True


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/", response_class=HTMLResponse, include_in_schema=False)
async def home(request: Request, user=Depends(manager), db: Session = Depends(get_db)):
    n = get_next_snapshot(db, user)
    context = {
        "request": request,
        "user": user,
    }
    if n:
        context.update({
            "snapshot_id": n.id,
            "remaining_snapshots": get_next_snap_query(db, user).count(),
            "snapshot_url": f"/api/snapshot/snapshot_images/{n.img_file_name}",
            "product_url": f"/api/snapshot/product_images/{n.ref_image_file_name}",
        })
    return templates.TemplateResponse("index.html", context)


@app.get("/login", response_class=HTMLResponse, include_in_schema=False)
async def login_page(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})


@app.post("/logged", response_class=HTMLResponse, include_in_schema=False)
async def logged_page(
        request: Request,
        form_data: OAuth2PasswordRequestForm = Depends(),
        db: Session = Depends(get_db)):
    try:
        token = login_user(db, form_data.username, form_data.password)[
            "access_token"]
    except Exception as e:
        return RedirectResponse(url="/login", status_code=status.HTTP_302_FOUND)
    response = RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)
    manager.set_cookie(response, token)
    return response


def login_user(db, username, password):
    db_user = db.query(dbUser).filter(
        dbUser.username == username).first()
    if not db_user:
        raise HTTPException(
            status_code=401, detail="Incorrect username or password")
    if not password == db_user.password:
        raise HTTPException(
            status_code=400, detail="Incorrect username or password")

    Sessions[f"{db_user.username}_token"] = db_user.username
    token = manager.create_access_token(
        data={'sub': f"{db_user.username}_token"})

    return {"access_token": token, "token_type": "bearer"}


@app.post("/api/login")
async def login(request: Request, form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    return login_user(db, form_data.username, form_data.password)


@app.post("/api/users/", response_model=User)
async def create_user(user: UserCreate, db: Session = Depends(get_db)):
    db_user = dbUser(username=user.username, password=user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def add_snap(snapshot: SnapshotCreate, db: Session = Depends(get_db)):
    db_snapshot = dbSnapshot(**snapshot.dict())
    db.add(db_snapshot)
    db.commit()
    db.refresh(db_snapshot)
    return db_snapshot


@app.post("/api/snapshot/", response_model=Snapshot)
async def create_snapshot(
        snapshot: SnapshotCreate,
        db: Session = Depends(get_db),
        user=Depends(manager)):
    return add_snap(snapshot, db)


def get_next_snap_query(db: Session, user_name: str):
    subquery = db.query(
        dbSnapshot.id,
        dbSnapshot.img_file_name,
        dbSnapshot.ref_image_file_name,
        dbUser.username
    ).outerjoin(dbVote, dbSnapshot.id == dbVote.snapshot_id)\
        .outerjoin(dbUser, dbVote.user_id == dbUser.id)\
        .group_by(dbSnapshot.id, dbUser.username)\
        .having(func.count(dbVote.id) < 3)\
        .subquery()

    return db.query(dbSnapshot.id, subquery.c.img_file_name, subquery.c.ref_image_file_name)\
        .join(dbSnapshot, subquery.c.id == dbSnapshot.id)\
        .outerjoin(dbUser, subquery.c.username == dbUser.username)\
        .filter((dbUser.username != user_name) | (dbUser.username == None))


def get_next_snapshot(db: Session, user_name: str):
    query = get_next_snap_query(db, user_name)
    next_snapshots = query.first()

    return next_snapshots


@app.get("/api/snapshot/remaining")
async def remaining_snapshot(
        db: Session = Depends(get_db),
        user=Depends(manager)):
    return get_next_snap_query(db, user).count()


@app.get("/api/snapshot/next", response_model=Snapshot)
async def next_snapshot(
        db: Session = Depends(get_db),
        user=Depends(manager)):
    return get_next_snapshot(db, user)


@app.get("/api/snapshot/", response_model=List[Snapshot])
async def all_snapshot(db: Session = Depends(get_db), user=Depends(manager)):
    user
    return db.query(dbSnapshot).all()


@app.get("/api/snapshot/{snapshot_id}", response_model=Snapshot)
async def get_snapshot(snapshot_id: int, db: Session = Depends(get_db)):
    return db.query(dbSnapshot).filter(
        dbSnapshot.id == snapshot_id).first()


@app.get("/api/snapshot/snapshot_images/{product}/{image_file}")
async def get_snapshot_image(product: str, image_file: str):
    return FileResponse(f"static/challenge_snapshots/{product}/{image_file}")


@app.get("/api/snapshot/product_images/{image_file}")
async def get_ref_image(image_file: str):
    return FileResponse(f"static/challenge_products/{image_file}")


@app.get("/api/vote/", response_model=List[Vote])
async def all_vote(db: Session = Depends(get_db)):
    data = db.query(dbVote).all()
    if data is None:
        return {"error": "Votes not found"}
    return data


def do_vote(db, option, snapshot_id, username):
    db_user = db.query(dbUser).filter(
        dbUser.username == username).first()
    db_vote = dbVote(
        option=option,
        snapshot_id=snapshot_id,
        user_id=db_user.id)
    db.add(db_vote)
    db.commit()
    db.refresh(db_vote)
    return db_vote


@app.post("/api/vote/", response_model=Vote)
async def create_vote(
        vote: Vote,
        db: Session = Depends(get_db),
        user=Depends(manager)):
    return do_vote(db, vote.option, vote.snapshot_id, user)


@app.get("/api/vote/{id}", response_model=Vote)
async def get_vote(id: str, db: Session = Depends(get_db)):
    db_vote = db.query(dbVote).filter(dbVote.id == id).first()
    if db_vote is None:
        return {"error": "Vote not found"}
    return db_vote


@app.get("/api/leaderboard", response_model=User)
async def next_snapshot(
        db: Session = Depends(get_db),
        user=Depends(manager)):
    subquery = (
        db.query(dbVote)
            .filter(dbVote.user_id == dbUser.id)
            .subquery()
    )

    users = (
        db.query(dbUser)
            .outerjoin(subquery, dbUser.id == subquery.c.user_id)
            .order_by(desc(subquery.c.id))
            .all()
    )
    return users
