# Data Cleaning Challenge

## Run

`docker-compose --env-file .env up --build`

Copy product images to `app/static/challenge_products`, snapshots images to related product folder in `app/tatic/challenge_snapshots` and run `python add_images.py` to load this images to app.

Create user via http://0.0.0.0:8000/docs#/default/create_user_api_users__post

Open in browser http://0.0.0.0:8000 or http://0.0.0.0:8000/docs
