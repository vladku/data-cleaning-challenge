import os
import requests

response = requests.post("http://0.0.0.0:8000/api/login", data={
    "username": "user",
    "password": "pass"
})
access_token = response.json()["access_token"]

headers = {"Authorization": f"Bearer {access_token}"}

def add_snap(img_file_name: str, ref_image_file_name: str):
    requests.post("http://0.0.0.0:8000/api/snapshot", json={
        "img_file_name": img_file_name,
        "ref_image_file_name": ref_image_file_name
    }, headers=headers)

p = f"{os.path.dirname(os.path.realpath(__file__))}/app/static/challenge_snapshots/"
for product in os.listdir(p):
    if product.startswith("."):
        continue
    for snap in os.listdir(f"{p}{product}/"):
        print(snap)
        add_snap(f"{product}/{snap}", f"{product}.jpg")